import java.util.Scanner;

public class Driver{
	public static void main (String[] args){
		System.out.println("Hello");
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Input 2 numbers, one at a time");
		int num1 = scan.nextInt();  
		int num2 = scan.nextInt(); 
		System.out.println("Sum is: " + Calculator.add(num1,num2));
		
		System.out.println("Input 1 number");
		int numRoot = scan.nextInt();  
		System.out.println("Square root is: " + Calculator.sqrt(numRoot));
		
		System.out.println("Here's a random number: " + Calculator.random());
	}
}